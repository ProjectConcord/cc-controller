module gitlab.com/ProjectConcord/cc-controller

go 1.15

require (
	github.com/arangodb/go-driver v0.0.0-20201106193344-56ae8fd24510
	github.com/bitwurx/jrpc2 v0.0.0-20200508153510-d8310ad1baf0
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/gofrs/uuid v3.3.0+incompatible
	github.com/kr/pretty v0.1.0 // indirect
	github.com/mitchellh/mapstructure v1.3.3
	github.com/stretchr/objx v0.1.1 // indirect
	github.com/stretchr/testify v1.6.1
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
)
